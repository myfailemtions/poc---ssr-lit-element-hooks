import { html } from 'lit-element'
import { component } from 'haunted'

const Card = () => html`
  <style>
    .ing-card {
      box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
      min-width: 320px;
      max-width: 448px;
      width: 100%;
      padding: 24px;
      border-radius: 4px;
    }
  </style>

  <div class="ing-card">
    <slot></slot>
  </div>
`

export default component(Card)
