import { html } from 'lit-element'
import { component } from 'haunted'
import eventDispatcher from '../utils/eventDispatcher'

const Button = (element) => {
  const { title } = element

  return html`
    <link rel="stylesheet" href="node_modules/materialize-css/dist/css/materialize.min.css">

    <style>
      .btn {
        background: #ff6200;
        border-radius: 4px;
      }
      .btn:hover {
        background: #ff6200;
      }
    </style>

    <a
      class="waves-effect waves-light btn"
      @click='${() => eventDispatcher(
        element,
        'handleClick',
        { title }
      )}'
    >
      ${title}
    </button>
  `
}

export default component(Button)
