import { html } from 'lit-element'
import { component } from 'haunted'
import eventDispatcher from '../utils/eventDispatcher'

const Input = (element) => {
  const { value, placeholder, name } = element

  return html`
    <link rel="stylesheet" href="node_modules/materialize-css/dist/css/materialize.min.css">

    <style>
      .input-field {
        width: 320px;
      }
    </style>

    <div class="input-field">
      <input
        @input="${
          ({ target }) => eventDispatcher(
            element,
            'handleInput',
            { value: target.value, name }
          )
        }"
        value="${value}"
        id="${name}"
        type="text"
      />
      <label
        class="active"
        for="${name}"
      >
        ${placeholder}
      </label>
    </div>
  `
}

Input.observedAttributes = ['name', 'placeholder', 'value', 'onInput']

export default component(Input)
