import { html } from 'lit-element'
import { component, useState } from 'haunted'
import eventDispatcher from '../utils/eventDispatcher'

const LoginForm = element => {
  const [username, setUserName] = useState(' ')
  const [password, setUserPassword] = useState(' ')

  const updateForm = ({ detail: { name, value } }) => {
    if (name === 'password') {
      setUserPassword(value)
    } else {
      setUserName(value)
    }
  }

  const submitForm = () =>
    eventDispatcher(
      element,
      'submitForm',
      { username, password }
    )

  return html`
    <form>
      <ing-input
        name="username"
        placeholder="User name"
        value="${username}"
        @handleInput="${updateForm}"
      ></ing-input>
      <ing-input
        name="password"
        placeholder="Password"
        value="${password}"
        @handleInput="${updateForm}"
      ></ing-input>
      <ing-button title="Log in" @handleClick="${submitForm}"></ing-button>
    </form>
  `
}

export default component(LoginForm)
