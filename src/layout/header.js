import { html } from 'lit-element'
import { component } from 'haunted'

const IngHeader = () => html`
  <link rel="stylesheet" href="node_modules/materialize-css/dist/css/materialize.min.css">
  <style>
    .brand-logo {
      width: 120px;
    }
  </style>
  <nav class="col s12 white">
    <div class="nav-wrapper container white">
      <ul id="nav" class="left hide-on-med-and-down">
        <a href="#" class="brand-logo">
          <img src="https://mijn.ing.nl/login/assets/images/svg/ing-logo.svg" />
        </a>
      </ul>
    </div>
  </nav>
`

export default component(IngHeader)