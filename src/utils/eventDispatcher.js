const eventDispatcher = (el, eventName, detail) => {
  const event = new CustomEvent(eventName, {
    bubbles: true,
    cancelable: false,
    detail
  })

  el.dispatchEvent(event)
}

export default eventDispatcher
