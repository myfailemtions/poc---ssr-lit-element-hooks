import { LitElement, html, css } from 'lit-element'
export default class App extends LitElement {

  render() {
    return html`
      <div id="app">
        <div class="header">
          <ing-header></ing-header>
        </div>
        <div class="content">
          <ing-login-page></ing-login-page>
        </div>
      </div>
    `
  }

  static get styles() {
    return css`
      .content {
        margin-top: 24px
      }
    `
  }
}
