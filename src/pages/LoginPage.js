import { LitElement, html, css } from 'lit-element'

export default class LoginPage extends LitElement {

  constructor() {
    super();
    this.loginForm = {}
  }

  handleSubmitForm({ detail }) {
    console.log(detail, 'important')
    this.loginForm = detail
    alert('Authorized')
  }

  render() {
    return html`
      <div class="app-login_page">
        <ing-card>
          <ing-login-form @submitForm="${this.handleSubmitForm}"></ing-login-form>
        </ing-card>
      </div>
    `
  }

  static get styles() {
    return css`
      .app-login_page {
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      }
    `
  }
}
