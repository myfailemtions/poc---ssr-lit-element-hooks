// components
import LoginPage from './pages/LoginPage'
import App from './pages/App'

// components
import Card from './components/Card'
import Input from './components/input'
import Button from './components/Button'
import LoginForm from './components/LoginForm'

// layout
import Header from './layout/Header'

// define
customElements.define('ing-input', Input)
customElements.define('ing-login-page', LoginPage)
customElements.define('ing-app', App)
customElements.define('ing-card', Card)
customElements.define('ing-header', Header)
customElements.define('ing-button', Button)
customElements.define('ing-login-form', LoginForm)
